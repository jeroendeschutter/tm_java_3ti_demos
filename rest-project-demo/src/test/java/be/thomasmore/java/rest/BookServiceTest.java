package be.thomasmore.java.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author JDCQ390
 */
public class BookServiceTest {
    
    public BookServiceTest() {
    }

    @Test
    public void readCsvFile() {
        File csvFile = new File("src/test/resources/books.csv");
        try {
            FileInputStream fis = new FileInputStream(csvFile);
            List<Book> books = new BookService().readBooksFromFile(fis);
            for (Book book : books) {
                System.out.println(book.toString());
            }
            assertTrue(true);
        } catch (FileNotFoundException ex) {
            fail(ex.getMessage());
        }
    }
    
}
