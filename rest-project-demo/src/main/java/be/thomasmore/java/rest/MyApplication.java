
package be.thomasmore.java.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/rest")
public class MyApplication extends ResourceConfig {

    public MyApplication() {
        super(MultiPartFeature.class);
        packages("be.thomasmore.java.rest"); // add all packages containing MultiPartFeature
    }
}
