package be.thomasmore.java.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Stateless
@Path("books")
public class BookService {

    @PersistenceContext(unitName = "bookstorePU")
    private EntityManager em;

    public List<Book> readBooksFromFile(InputStream csvFile) {

        List<Book> books = new ArrayList<>();
        String line;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(csvFile))) {
            line = br.readLine();
            while (line != null) {
                String[] columns = line.split(",");
                Book book = new Book(columns[0], columns[1], Integer.valueOf(columns[2]));
                books.add(book);
                line = br.readLine();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return books;
    }

    @POST
    @Path("uploadCsv")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public List<Book> readBooksFromFileAndStore(@FormDataParam("file") InputStream csvFile,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
        List<Book> books = storeBooks(readBooksFromFile(csvFile));
        return books;
    }

    public List<Book> storeBooks(List<Book> books) {
        for (Book book : books) {
            em.persist(book);
        }
        return books;
    }

    @GET
    @Path("{bookId}")
    public Book findBookById(@PathParam("bookId") Long bookId) {
        return em.find(Book.class, bookId);
    }

    @GET
    @Path("all")
    public List<Book> findAllBooks() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Book.class));
        return em.createQuery(cq).getResultList();
    }

}
