package be.thomasmore.java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;
import static org.junit.Assert.*;

public class FileReadTest {
    
     @Test
     public void readFileContents() {
        try {
            File file = new File("src/test/resources/input.txt");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String string = br.readLine();
            assertEquals("Java", string);
            br.close();
        } catch (IOException ex) {
            fail(ex.getMessage());
        }        
     }
}
